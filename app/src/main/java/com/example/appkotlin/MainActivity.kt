package com.example.appkotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {

    private lateinit var txtUserLog : EditText
    private lateinit var txtPassword : EditText
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        iniciarComponentes()

        eventoClick()





        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun iniciarComponentes(){

        txtUserLog = findViewById(R.id.txtUserLog)
        txtPassword = findViewById(R.id.txtPassword)
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)


    }

    public fun eventoClick(){
        btnIngresar.setOnClickListener(View.OnClickListener {

            var usuario : String = getString(R.string.user)
            var pass : String = getString(R.string.pass)
            var nombre : String = getString(R.string.nombre)

            if(txtUserLog.text.toString().contentEquals(usuario) &&
                txtPassword.text.toString().contentEquals(pass)){

                val intent = Intent(this, OperacionesActivity::class.java)
                intent.putExtra("usuario", nombre)

                txtUserLog.setText("")
                txtPassword.setText("")
                startActivity(intent)
            }else{
                Toast.makeText(this, "Información faltante / Incorrecta", Toast.LENGTH_SHORT).show()
            }


        })

        btnSalir.setOnClickListener(View.OnClickListener {
            val alertBuilder = AlertDialog.Builder(this@MainActivity)

            alertBuilder.setMessage("¿Desea limpiar los valores y resultados?")
            alertBuilder.setTitle("Salir de la aplicación")
            alertBuilder.setPositiveButton("Si"){_,_ ->
                finish()
            }

            alertBuilder.setNeutralButton("No"){_,_ ->}

            val box = alertBuilder.create()
            box.show()
        })
    }
}