package com.example.appkotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {

    private lateinit var txtUsuario : TextView
    private lateinit var txtNumA : EditText
    private lateinit var txtNumB : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSuma : Button
    private lateinit var btnResta : Button
    private lateinit var btnMulti : Button
    private lateinit var btnDivision : Button

    private lateinit var btnLimpiar : Button
    private lateinit var btnSalir : Button

    private lateinit var ops : Operaciones

    var Opcion : Int =0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)

        iniciarComponentes()
        eventoClick()


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    public fun iniciarComponentes(){

        txtUsuario = findViewById(R.id.txtUsuario)
        txtNumA = findViewById(R.id.txtNumA)
        txtNumB = findViewById(R.id.txtNumB)
        txtResultado = findViewById(R.id.txtResultado)

        btnSuma = findViewById(R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)
        btnMulti = findViewById(R.id.btnMulti)
        btnDivision = findViewById(R.id.btnDivision)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnSalir = findViewById(R.id.btnSalir)

        val bundle : Bundle? = intent.extras
        txtUsuario.text = "Usuario: " + bundle?.getString("usuario")


    }

    public fun validar() : Boolean{
        if ( txtNumA.text.toString().contentEquals("") ||
            txtNumB.text.toString().contentEquals("")){
            return false
        }
        else return true
    }

    public fun eventoClick(){
        btnSuma.setOnClickListener(View.OnClickListener {
            if (!validar()){
                Toast.makeText(this, "Ingrese el numero faltante", Toast.LENGTH_SHORT).show()
            }
            else{
                val numA = txtNumA.text.toString().toFloat()
                val numB = txtNumB.text.toString().toFloat()

                ops=Operaciones(numA, numB)
                txtResultado.text = "Resultado: "+ ops.sumar().toString()
            }
        })

        btnResta.setOnClickListener(View.OnClickListener {
            if (!validar()){
                Toast.makeText(this, "Ingrese el numero faltante", Toast.LENGTH_SHORT).show()
            }
            else{
                val numA = txtNumA.text.toString().toFloat()
                val numB = txtNumB.text.toString().toFloat()

                ops=Operaciones(numA, numB)
                txtResultado.text = "Resultado: "+ops.resta().toString()
            }
        })

        btnMulti.setOnClickListener(View.OnClickListener {
            if (!validar()){
                Toast.makeText(this, "Ingrese el numero faltante", Toast.LENGTH_SHORT).show()
            }
            else{
                val numA = txtNumA.text.toString().toFloat()
                val numB = txtNumB.text.toString().toFloat()

                ops=Operaciones(numA, numB)
                txtResultado.text = "Resultado: "+ ops.multi().toString()
            }
        })

        btnDivision.setOnClickListener(View.OnClickListener {
            if (!validar()){
                Toast.makeText(this, "Ingrese el numero faltante", Toast.LENGTH_SHORT).show()
            }else if(txtNumB.text.toString().toFloat() == 0f){
                Toast.makeText(this, "No posible dividir un número entre 0, intente otra vez.", Toast.LENGTH_SHORT).show()


            }
            else{
                val numA = txtNumA.text.toString().toFloat()
                val numB = txtNumB.text.toString().toFloat()

                ops=Operaciones(numA, numB)
                txtResultado.text = "Resultado: " + ops.division().toString()
            }
        })

        btnSalir.setOnClickListener(View.OnClickListener {

            val alertBuilder = AlertDialog.Builder(this@OperacionesActivity)

            alertBuilder.setMessage("¿Desea limpiar los valores y resultados?")
            alertBuilder.setTitle("Cerrar sesión")
            alertBuilder.setPositiveButton("Si"){_,_ ->
                finish()
            }

            alertBuilder.setNeutralButton("No"){_,_ ->}

            val box = alertBuilder.create()
            box.show()
        })

        btnLimpiar.setOnClickListener(View.OnClickListener {

            val alertBuilder = AlertDialog.Builder(this@OperacionesActivity)

            alertBuilder.setMessage("¿Desea limpiar los valores y resultados?")
            alertBuilder.setTitle("Limpiar")
            alertBuilder.setPositiveButton("Si"){_,_ ->
                txtResultado.text = "Resultado: "
                txtNumA.setText("")
                txtNumB.setText("")
            }

            alertBuilder.setNeutralButton("No"){_,_ ->}

            val box = alertBuilder.create()
            box.show()
        })


    }

}